import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "",
  routes: [
    {
      path: "/",
      redirect: "/popular"
    },
    {
      path: "/popular",
      name: "popular movies",
      component: () => import("../views/Popular")
    },
    {
      path: "/toprated",
      name: "top rated movies",
      component: () => import("../views/TopRated")
    },
    {
      path: "/movie/:movie",
      name: "current movie",
      component: () => import("../views/CurrentMovie")
      },
  ]
});